import { Component, OnInit, Input } from '@angular/core';
import { EntityService } from '../services/entity.service'

@Component({
  selector: 'app-pager',
  templateUrl: './pager.component.html',
  styleUrls: ['./pager.component.css']
})
export class PagerComponent implements OnInit {
  @Input() entityService:EntityService;
  private pages:number[]
  private first:boolean
  private last:boolean
  private current:number
  constructor() { }

  ngOnInit() {
    if(this.entityService.promise){
      this.entityService.promise.then(results=>{this.update()})
    }
  }
  
  update(){
    let p = Math.ceil(this.entityService.total_results / this.entityService.filter.limit);
    this.pages = Array(p).fill(1).map((x,i)=>i+1);
    this.current = this.entityService.filter.page
    this.first = this.current === 1
    this.last = this.current === p
  }
  next(){
    this.entityService.next().then(results=>{this.update()})
  }
  prev(){
    this.entityService.prev().then(results=>{this.update()})
  }
  navigate(page){
    this.entityService.navigate(page).then(results=>{this.update()})
  }
}

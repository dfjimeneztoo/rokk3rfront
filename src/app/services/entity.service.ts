import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { environment } from '../../environments/environment';
import 'rxjs/add/operator/toPromise';
import { Response } from '@angular/http/src/static_response';
class Filter{
  filter: string
  order: string
  properties: any
  limit: number
  page: number
  simple: boolean
}
export class EntityService{
  private url = environment.API;
  public filter:Filter;
  private bookmark:number;
  private selected:any[];
  public total_results:number;
  public data:any[];
  private _promise:any;
  constructor(private http: Http) { 
    this.total_results = Number.MAX_SAFE_INTEGER; 
    this.bookmark = 1;
    this.filter = new Filter();
    this.filter.filter = '';
    this.filter.page = 1
    this.filter.limit = 10;
    this.filter.properties = {}
    this.filter.simple = true
    this.selected = [];
  }
  get promise(){
    return this._promise
  }

  //====== LOADING ENTITIES METHODS =====//
  /**
   * loads configuration for the entity
   * @param endpoint the endpoint which will use to create,request,update,delete data
   */
  loadEntity(endpoint:string){
    this.url = environment.API + endpoint
    this.reset();
  }
  //====== GET AND SEARCH METHODS =====//
  reset() {
    this.selected = [];
    this.filter.page = 1;
    this.filter.properties = {}
    this.total_results = Number.MAX_SAFE_INTEGER;
    this.data = []
  }
  clear(): Promise<Response> {
    this.selected = [];
    this.filter.page = this.bookmark;
    return this.getData();
  }
  setFilterText(search:string): Promise<Response> {
    if(search.length == 0){
      this.clear();
    }
    this.bookmark = this.filter.page;
    this.filter.filter = search;
    this.filter.page = 1;
    return this.getData();
  }
  next(): Promise<Response> {
    if(this.filter.page * this.filter.limit < this.total_results){
      this.filter.page +=1;
    }
    return this.getData();
  }
  navigate(page): Promise<Response> {
    this.filter.page = page;
    return this.getData();
  }
  prev(): Promise<Response> {
    if(this.filter.page > 1){
      this.filter.page -=1;
    }
    return this.getData();
  }
  getData(): Promise<Response> {
    let url = this.url
    let params = []
    if(this.filter.filter.length > 0){
      params.push('filter='+this.filter.filter)
    }
    for(let i in this.filter.properties){
      if(this.filter.properties[i] instanceof Array){
        let array = this.filter.properties[i]
        array.forEach(element => {
          params.push('filter_field='+i+'&filter_value='+element)  
        });
      }else{
        params.push('filter_field='+i+'&filter_value='+this.filter.properties[i])
      }
    }
    console.log('order')
    if(this.filter.order){
      params.push('order='+this.filter.order)
    }
    params.push('limit='+this.filter.limit)
    params.push('page='+this.filter.page)
    params.push('simple='+this.filter.simple.toString())
    this._promise = this.http.get(this.url+'?'+params.join('&'))
    .toPromise()
    this._promise.then(response => {
                 let res = response.json()
                 this.total_results = res.total_results
                 this.data = res.data
                 return this.data
               } )
               .catch(this.handleError);
    return this._promise
  }
  
  /**
   * SAVE DATA METHODS
   */
  saveData(data:any){
    const headers = new Headers({});
    let options = new RequestOptions({ headers });
    const formData = new FormData();
    for(let i in data){
      formData.append(i, data[i]);
    }
    this._promise = this.http.post(this.url+'/create',formData,options).toPromise()
    return this._promise
  }

  /**
   * UPDATE DATA METHODS
   */
  updateData(data:any):Promise<any>{
    const headers = new Headers({});
    let options = new RequestOptions({ headers });
    const formData = new FormData();
    for(let i in data){
      formData.append(i, data[i]);
    }
    this._promise = this.http.put(this.url+'/update',formData,options).toPromise();
    return this._promise
  }

  /**
   * DELETE DATA METHODS
   */
  deleteData = function (data:any) {
    return this.http.delete(this.url+'/destroy/'+data.id).toPromise()
  }

  //====== ERROR CATCHING METHODS =====//
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
@Injectable()
export class EntityServiceFactory {
  constructor(private http: Http) { 
  }
  loadEntity(endpoint:string):EntityService{
    let service = new EntityService(this.http);
    service.loadEntity(endpoint);
    return service;
  }
}

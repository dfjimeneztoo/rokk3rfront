import { Component } from '@angular/core';
import { EntityServiceFactory } from './services/entity.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers:[EntityServiceFactory]
})
export class AppComponent {
  title = 'Rokk3r Tasks';
}

import { Component, OnInit } from '@angular/core';
import { EntityService, EntityServiceFactory } from '../services/entity.service';
declare var $:any;
class Task{
  name:string
  priority:number
  dueDate:Date
}
@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {

  private task
  private dueTaskService:EntityService
  private pendingTaskService:EntityService
  private now
  constructor(private serviceFactory:EntityServiceFactory) {
    this.task = new Task(); 
    this.now = new Date();
  }

  ngOnInit() {
    this.dueTaskService = this.serviceFactory.loadEntity('/task')
    this.dueTaskService.filter.properties.dueDate = '< '+this.now.toISOString()
    this.dueTaskService.getData()

    this.pendingTaskService = this.serviceFactory.loadEntity('/task')
    this.pendingTaskService.filter.properties.dueDate = '> '+this.now.toISOString()
    this.pendingTaskService.getData()
  }
  sortDue(property){
    let sort = property + ' asc'
    if(this.dueTaskService.filter.order === sort){
      sort = property + ' desc'
    }
    this.dueTaskService.filter.order = sort
    this.dueTaskService.getData()
  }
  sortPending(property){
    let sort = property + ' asc'
    if(this.pendingTaskService.filter.order === sort){
      sort = property + ' desc'
    }
    this.pendingTaskService.filter.order = sort
    this.pendingTaskService.getData()
  }

  addTask(event){
    this.task = new Task();
    $('#create').modal('show')
  }
  saveTask(){
    this.dueTaskService.saveData(this.task).then((response)=>{
      this.dueTaskService.getData()
      this.pendingTaskService.getData()
    })
    $('#create').modal('hide')
  }
}
